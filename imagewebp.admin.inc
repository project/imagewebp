<?php

/**
 * @file
 * ImageWebP admin functions.
 */

/**
 * Page callback for admin form.
 */
function imagewebp_admin_form() {
  $form['imagewebp_quality'] = array(
    '#type' => 'textfield',
    '#title' => t('ImageWebP quality'),
    '#field_suffix' => '%',
    '#required' => TRUE,
    '#description' => t('Determines the quality of the generated .webp images'),
    '#default_value' => variable_get('imagewebp_quality', 80),
    '#attributes' => array(
      'size' => 4,
    ),
  );
  $form['#submit'][] = 'imagewebp_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Validate handler for imagewebp_admin_form().
 */
function imagewebp_admin_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['imagewebp_quality'])) {
    form_set_error('imagewebp_quality', t('Quality must be numeric.'));
  }
  else {
    if ($form_state['values']['imagewebp_quality'] <= 0 || $form_state['values']['imagewebp_quality'] > 100) {
      form_set_error('imagewebp_quality', t('Quality percentage must be between 0 and 100.'));
    }
  }
}

/**
 * Submit handler for imagewebp_admin_form().
 */
function imagewebp_admin_form_submit($form, &$form_state) {
  drupal_set_message(t('ImageWebP quality has been set to !percent%', array('!percent' => $form_state['values']['imagewebp_quality'])));
}

/**
 * Page callback for admin/config/media/imagewebp/generate.
 */
function imagewebp_setup_batch_form($form, &$form_state) {
  $fields = field_info_field_map();
  $image_fields = array_filter($fields, function($key) {
    return ($key['type'] == 'image');
  });
  $image_bundles = array();
  if (!empty($image_fields)) {
    foreach ($image_fields as $image_field => $data) {
      if (!empty($data['bundles'])) {
        foreach ($data['bundles'] as $type => $instances) {
          foreach ($instances as $instance) {
            $info = field_info_instance($type, $image_field, $instance);
            if (!empty($info)) {
              foreach ($info['display'] as $display) {
                if (array_key_exists('webp_support', $display['settings']) && $display['settings']['webp_support']) {
                  $image_bundles[$image_field][$instance] = $info['label'];
                }
              }
            }
          }
        }
      }
    }
  }
  if (!empty($image_bundles)) {
    foreach ($image_bundles as $field => $bundles) {
      foreach ($bundles as $bundle => $label) {
        $suffix = t('(in %bundle content type)', array('%bundle' => $bundle));
        $field_options[$field . '|' . $bundle] = $label . " " . $suffix;
      }
    }
    $form['imagewebp_image_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t("Which fields should be checked for generating WebP images?"),
      '#options' => $field_options,
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Start process'),
    );
  }
  else {
    $form['no_image_field'] = array(
      '#type' => 'markup',
      '#markup' => t("There isn't any configured image field which uses Webp images."),
    );
  }

  return $form;
}

/**
 * Submit handler for imagewebp_setup_batch_form().
 */
function imagewebp_setup_batch_form_submit($form, &$form_state) {
  $chosen_fields = array_filter($form_state['values']['imagewebp_image_fields'], function ($val, $key) {
    return $val !== 0;
  }, ARRAY_FILTER_USE_BOTH);

  $operations = array();
  foreach ($chosen_fields as $field) {
    $fields = explode('|', $field);
    $operations[] = array('imagewebp_generate_webp_images_batch_process', array($fields[0], $fields[1]));
  }

  $batch = array(
    'title' => 'Generate WebP images',
    'operations' => $operations,
    'finished' => 'imagewebp_generate_webp_images_batch_finished',
  );
  batch_set($batch);
}
