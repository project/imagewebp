<?php
/**
 * @file
 * Theme functions for ImageWebP module.
 */

/**
 * Theme function based on theme_image_formatter().
 *
 * It wraps the <img> tag in a <picture> tag, and adds a <source> tag containing
 * the webp image.
 *
 * This method provides a fallback for browsers that do not support WebP images,
 * while still using the same markup across all browsers. This allows caching to
 * be enabled safely.
 */
function theme_imagewebp_formatter(&$variables) {
  $item = $variables['item'];

  $image = array(
    'path' => $item['uri'],
  );
  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }
  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }
  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  // Create a WebP version of the image.
  if (isset($item['webp_uri']) && !empty($item['webp_uri'])) {
    $webp_image = $image;
    $webp_image['path'] = $item['webp_uri'];
    $webp_image['type'] = 'image/webp';
  }

  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];

    $output = theme('image_style', $image);
    if (_imagewebp_webp_server_support()) {
      $source = theme('imagewebp_source_style', $image);
    }
  }

  else {
    $output = theme('image', $image);
    if (isset($webp_image)) {
      $source = theme('imagewebp_source', $webp_image);
    }
  }

  // Create WebP output.
  if (isset($source)) {
    $picture  = '<picture>';
    $picture .= '  ' . $source;
    $picture .= '  ' . $output;
    $picture .= '</picture>';

    $output = $picture;
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();

    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}

/**
 * Returns HTML for a source tag within the picture element.
 *
 * @param $variables
 *   An associative array containing:
 *   - path: The path or URI of the image file (relative to base_path()) or a
 *       full URL.
 *   - width: The width of the image (if known).
 *   - height: The height of the image (if known).
 *   - alt: The alternative text for text-based browsers.
 *   - title: Text displayed on hover in some browsers.
 *   - type: The internet media type (formerly known as MIME type).
 *   - attributes: Associative array of attributes to be placed in the tag.
 *
 * @return string
 */
function theme_imagewebp_source($variables) {
  $attributes = $variables['attributes'];
  $attributes['srcset'] = file_create_url($variables['path']);
  foreach (array('width', 'height', 'alt', 'title', 'type') as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  return '<source' . drupal_attributes($attributes) . ' />';
}

/**
 * Returns HTML for an source tag using a specific image style.
 *
 * @param $variables
 *   An associative array containing:
 *   - style_name: The name of the style to be used to alter the original image.
 *   - path: The path or URI of the image file (relative to base_path()) or a
  *       full URL.
  *   - width: The width of the image (if known).
  *   - height: The height of the image (if known).
  *   - alt: The alternative text for text-based browsers.
  *   - title: Text displayed on hover in some browsers.
  *   - type: The internet media type (formerly known as MIME type).
  *   - attributes: Associative array of attributes to be placed in the tag.
 *
 * @return string
 */
function theme_imagewebp_source_style($variables) {
  // Determine the URI for the styled image, and confirm it exists.
  $style_uri = image_style_path($variables['style_name'], $variables['path']);
  if (file_exists($style_uri)) {

    // Create the WebP Version of the styled image.
    $webp_filepath = _imagewebp_create_webp($style_uri);
    if ($webp_filepath) {
      // Set the WebP path.
      $variables['path'] = $webp_filepath;

      // Determine the dimensions of the styled image.
      $dimensions = array(
        'width' => $variables['width'],
        'height' => $variables['height'],
      );
      image_style_transform_dimensions($variables['style_name'], $dimensions);
      $variables['width'] = $dimensions['width'];
      $variables['height'] = $dimensions['height'];

      return theme('imagewebp_source', $variables);
    }
  }

  return '';
}
